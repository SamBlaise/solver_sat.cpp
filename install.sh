#!/bin/bash
# Version 0.1

usage(){
    clear
    echo "Usage : Ce script a pour fonction d'automatiser l'installation de packages nécessaires au fonctionnement du projet.";
    echo "Attention : L'installation des différents packages nécessitent une connexion internet.";
    echo "Information : Le gestionnaire de paquets Hombrew est nécessaire pour l'installation des différents packages.";
    echo "Le gestionnaire de paquets Hombrew est donc automatiquement installé s'il n'est pas présent.";
    echo -e "\nInstallation / Désinstallation : L'installation et la désinstallation des packages nécessaires comprend notamment : ";
    echo -e "\n- Boost";
    echo -e "- Cmake\n";

}

menu(){
    echo -e "\n MENU\n"
    echo " 1. Installation de Hombrew ainsi que des packages nécessaires (4. Informations / Usage)"
    echo " 2. Désinstallation des différents packages installés"
    echo " 3. Désinstallation de Hombrew"
    echo " 4. Informations / Usage"
    echo -e " 5. Quitter"
    echo -n "Entrez votre choix : "
}

brewInstallingUninstalling(){

    if [ $1 == 0 ] ; 
    then 
        if [[ $(command -v brew) == "" ]]; then
            echo "Installing Hombrew"
            /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
        else
            echo "Updating Homebrew"
            brew update
        fi

    elif [ $1 == 1 ] ; 
    then 
        if [[ $(command -v brew) != "" ]]; then
            echo "Uninstalling Hombrew"
            /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall)"
        else
            echo "Homebrew is already uninstall"
            brew update
        fi
    fi
}

brewPackageInstalling(){
    if brew list -1 | grep -q "$1"; then
        echo "Package $1 is installed"
    else
        echo "Package $1 is not installed"
        echo "Installing $1"
        brew install $1
    fi
}

brewPackageUninstalling(){
    if brew list -1 | grep -q "$1"; then
        echo "Package $1 is installed"
        echo "Uninstalling $1"
        brew uninstall $1
    else
        echo "Package $1 is already uninstall"
    fi
}


boolc=0
 
if [ $# == 0 ] ;
then
   clear 
   brewInstallingUninstalling 0
   brewPackageInstalling "boost"
   brewPackageInstalling "cmake"
else    
    clear
    while [ boolc == 0 ] ;
    menu
    read answer 
    do
        case "$answer" in
            1)  brewInstallingUninstalling 0
                brewPackageInstalling "boost"
                brewPackageInstalling "cmake" ;;

            2) brewPackageUninstalling "boost"
               brewPackageUninstalling "cmake" ;;

            3)  brewInstallingUninstalling 1 ;;

            4) usage ;;

            5) exit ;;
            
            *) echo "Sélection non valide : $answer" 
               echo "Merci de bien vouloir réitérer votre choix" ;;
        esac
    done
fi
