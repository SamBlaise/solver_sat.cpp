//
// Created by sam on 08/10/18.
//

#include <set>
#include "Sat.hpp"

Sat::Sat(int number_of_variables, const std::vector<Formula> &clauses) : number_of_variables(number_of_variables),
                                                                         clauses(clauses)
{}


Formula::Formula(const Literal var, const size_t num_of_vars) : Formula(num_of_vars)
{
    this->flip(var);
}

Formula::Formula(boost::dynamic_bitset<> lit, boost::dynamic_bitset<> neg_lit)
{
    this->lit = lit;
    this->neg_lit = neg_lit;
}

Formula::Formula(size_t num_of_vars)
{
    this->lit = boost::dynamic_bitset<>(num_of_vars);
    this->neg_lit = boost::dynamic_bitset<>(num_of_vars);
}

std::set<Literal> Formula::to_set() const
{
    auto res = std::set<Literal>();
    for (size_t i = 0; i < this->lit.size(); ++i)
    {
        if (this->lit[i])
        {
            res.insert(i + 1);
        }
        if (this->neg_lit[i])
        {
            res.insert(-(i + 1));
        }
    }
    return res;
}



