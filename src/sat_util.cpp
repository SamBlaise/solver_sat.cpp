//
// Created by sam on 08/10/18.
//

#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/dynamic_bitset.hpp>
#include <queue>
#include "sat_util.hpp"
#include "Sat.hpp"

using namespace std;

Sat load_sat_from_dimacs(std::string path_to_dimacs_file)
{
    ifstream is(path_to_dimacs_file);
    auto number_of_variables = 0u;
    vector<Formula> clauses;
    if (is.is_open())
    {
        string line;
        bool first_line = true;
        while (getline(is, line))
        {
            if (line[0] == 'c' || line[0] == '%' || line[0] == '0' || line.empty())
                continue;
            vector<string> line_splitted;
            if (first_line)
            {
                boost::split(line_splitted, line, boost::is_any_of(" "));
                number_of_variables = stoi(line_splitted[2]);
                first_line = false;
            } else
            {
                boost::split(line_splitted, line, boost::is_any_of(" "));
                boost::dynamic_bitset<> lit(number_of_variables, 0);
                boost::dynamic_bitset<> neg_lit(number_of_variables, 0);
                for (const auto &word : line_splitted)
                {
                    if (word.empty())
                        continue;
                    auto value = stoi(word);
                    if (value == 0)
                        break;
                    if (value > 0)
                        lit[value - 1] = true;
                    else
                        neg_lit[(-value) - 1] = true;

                }
                clauses.emplace_back(std::move(lit), std::move(neg_lit));
            }
        }
        return std::move(Sat(number_of_variables, clauses));
    } else
    {
        throw std::invalid_argument("Unable to open the file " + path_to_dimacs_file);
    }
}

bool test_model(const Formula &model, const Sat &sat)
{
    for (const auto &f: sat.clauses)
        if ((model.lit & f.lit).empty() && (model.neg_lit & f.neg_lit).empty())
            return false;
    return true;
}


