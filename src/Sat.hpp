//
// Created by sam on 08/10/18.
//

#ifndef SOLVER_SAT_CPP_SAT_HPP
#define SOLVER_SAT_CPP_SAT_HPP

#include <list>
#include <exception>
#include <boost/dynamic_bitset.hpp>
#include <set>

using Literal=long;

struct Formula
{
    boost::dynamic_bitset<> lit;
    boost::dynamic_bitset<> neg_lit;

    Formula(boost::dynamic_bitset<> lit, boost::dynamic_bitset<> neg_lit);
    Formula(size_t num_of_vars);
    Formula(Literal var, const size_t num_of_vars);

    ~Formula()
    {
        lit.clear();
        neg_lit.clear();
    }

    inline bool is_unit() const;

    inline bool is_empty() const;

    inline Literal get_unit() const;

    inline bool operator[](Literal) const;

    inline Formula & flip(Literal);

    std::set<Literal> to_set() const;
};

struct Sat
{
    size_t number_of_variables;
    std::vector<Formula> clauses;

    Sat(int number_of_variables, const std::vector<Formula> &clauses);
};


bool Formula::is_unit() const
{
    return (this->lit.count() == 1 && this->neg_lit.none()) || (this->neg_lit.count() == 1 && this->lit.none());
}


bool Formula::is_empty() const
{
    return this->lit.none() && this->neg_lit.none();
}

Literal Formula::get_unit() const
{
    if (!this->is_unit())
        throw std::logic_error("The clause is not unit");
    if (this->lit.count() == 1)
        return lit.find_first() + 1;
    return -(neg_lit.find_first() + 1);
}

bool Formula::operator[](Literal index) const
{
    const size_t i = abs(index) - 1;
    if (index == 0)
        throw std::logic_error("Cannot get 0 literal");
    if (index < 0)
        return this->neg_lit[i];
    return this->lit[i];
}

Formula & Formula::flip(Literal index)
{
    const size_t i = abs(index) - 1;
    if (index == 0)
        throw std::logic_error("Cannot get 0 literal");
    if (index < 0)
        neg_lit.flip(i);
    else
        lit.flip(i);
    return *this;
}



#endif //SOLVER_SAT_CPP_SAT_HPP
