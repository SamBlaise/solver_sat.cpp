//
// Created by sam on 08/10/18.
//

#ifndef SOLVER_SAT_CPP_SAT_FUNCTIONS_HPP
#define SOLVER_SAT_CPP_SAT_FUNCTIONS_HPP

#include <iostream>
#include <optional>
#include "Sat.hpp"

bool test_model(const Formula &model, const Sat &sat);

Sat load_sat_from_dimacs(std::string path_to_dimacs_file);

#endif //SOLVER_SAT_CPP_SAT_FUNCTIONS_HPP
