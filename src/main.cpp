#include <iostream>
#include "sat_util.hpp"
#include "solver_sat.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    if (argc <= 1)
        throw std::invalid_argument("not enough arguments");
    for (int i = 1; i < argc; ++i)
    {
        try
        {
            auto sat = load_sat_from_dimacs(argv[i]);
            const auto result = solver_sat(sat);
            const auto sat_or_nsat = result.has_value() ? "SATISFIABLE" : "UNSATISFIABLE";
            cout << "s" << " " << sat_or_nsat << endl;
            if (result.has_value())
            {
                cout << "v" << " ";
                for (const auto &str:result->to_set())
                    cout << str << " ";
                cout << endl;
            }
        } catch (std::exception &e)
        {
            cerr << e.what() << endl;
        }
    }
}